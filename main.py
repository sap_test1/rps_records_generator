import random

def generate_records(file_path, num_records):
    records = ["R", "P", "S"]

    with open(file_path, "w") as file:
        for record in range(num_records):
            record = random.choice(records)
            file.write(record + "\n")

generate_records("player1.txt", 100)
generate_records("player2.txt", 100)